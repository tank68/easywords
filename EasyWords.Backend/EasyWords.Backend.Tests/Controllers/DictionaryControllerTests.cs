﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Web.Http;
using AutoMapper;
using EasyWords.Backend.Controllers;
using EasyWords.Backend.Database;
using EasyWords.Backend.Database.Entities;
using EasyWords.Backend.Models;
using EasyWords.Backend.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Ploeh.AutoFixture;

namespace EasyWords.Backend.Tests.Controllers
{
    [TestClass]
    public class DictionaryControllerTests
    {
        private DictionariesController _dictionaryController;
        private IUnitOfWork _unitOfWork;
        private EasyWordsContext _easyWordsContext;
        private IUserInfo _userInfo;
        private DictionaryEntity _dictionaryEntity;

        [TestInitialize]
        public void TestInitialize()
        {
            Bootstrapper.ConfigureAutoMapper();
            _easyWordsContext = Substitute.For<EasyWordsContext>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _userInfo = new UserInfoMock();
            _userInfo.UserId = "10";
            _dictionaryController = new DictionariesController(_unitOfWork, _userInfo);

            _dictionaryEntity = new Fixture().Create<DictionaryEntity>();
            _dictionaryEntity.UserId = _userInfo.UserId;
            _unitOfWork.DictionariesRepository = Substitute.For<GenericRepository<DictionaryEntity>>(_easyWordsContext);
            _unitOfWork.DictionariesRepository.GetById(_dictionaryEntity.Id).Returns(_dictionaryEntity);

        }

        [TestMethod]
        public void Get()
        {
            IQueryable<DictionaryEntity> entites = new EnumerableQuery<DictionaryEntity>(new List<DictionaryEntity>() { _dictionaryEntity });
            _unitOfWork.DictionariesRepository.Where(Arg.Any<Expression<Func<DictionaryEntity, bool>>>()).Returns(entites);

            var returnedEntities = _dictionaryController.Get();

            Assert.AreEqual(entites.ToList()[0].Id, returnedEntities.ToList()[0].Id);
            Assert.AreEqual(entites.ToList()[0].Title, returnedEntities.ToList()[0].Title);
            Assert.AreEqual(entites.ToList()[0].Description, returnedEntities.ToList()[0].Description);
        }

        [TestMethod]
        public void GetById()
        {
            _dictionaryController.Get(_dictionaryEntity.Id);
            _unitOfWork.DictionariesRepository.Received().GetById(_dictionaryEntity.Id);
        }

        [TestMethod]
        public void GetByIdNotFound()
        {
            try
            {
                _dictionaryController.Get(1);

                Assert.Fail("Expected Exception was not thrown");
            }
            catch (HttpResponseException ex)
            {
                Assert.AreEqual(HttpStatusCode.NotFound, ex.Response.StatusCode);
            }
        }

        [TestMethod]
        public void GetByIdNoPermission()
        {
            try
            {
                var dictionaryEntity = new Fixture().Create<DictionaryEntity>();
                dictionaryEntity.UserId = "100";
                _unitOfWork.DictionariesRepository.GetById(dictionaryEntity.Id).Returns(dictionaryEntity);

                _dictionaryController.Get(dictionaryEntity.Id);

                Assert.Fail("Expected Exception was not thrown");
            }
            catch (HttpResponseException ex)
            {
                Assert.AreEqual(HttpStatusCode.Forbidden, ex.Response.StatusCode);
            }
        }

        [TestMethod]
        public void Delete()
        {
            _dictionaryController.Delete(_dictionaryEntity.Id);
            _unitOfWork.DictionariesRepository.Received().Delete(_dictionaryEntity.Id);
        }

        [TestMethod]
        public void DeleteNotFound()
        {
            try
            {
                _dictionaryController.Delete(6);

                Assert.Fail("Expected Exception was not thrown");
            }
            catch (HttpResponseException ex)
            {
                Assert.AreEqual(HttpStatusCode.NotFound, ex.Response.StatusCode);
            }
        }

        [TestMethod]
        public void DeleteNoPermission()
        {
            var dictionaryEntity = new Fixture().Create<DictionaryEntity>();
            dictionaryEntity.UserId = "100";
            _unitOfWork.DictionariesRepository.GetById(dictionaryEntity.Id).Returns(dictionaryEntity);

            try
            {
                _dictionaryController.Delete(dictionaryEntity.Id);
            }
            catch (HttpResponseException ex)
            {
                Assert.AreEqual(HttpStatusCode.Forbidden, ex.Response.StatusCode);
            }
        }

        [TestMethod]
        public void Post()
        {
            var dictionaryViewModel = new DictionaryViewModel();

            _dictionaryController.Post(dictionaryViewModel);
            _unitOfWork.DictionariesRepository.Received().Insert(Arg.Any<DictionaryEntity>());
        }

        [TestMethod]
        public void PutAndCheckUpdate()
        {

            var dictionaryViewModel = Mapper.Map<DictionaryViewModel>(_dictionaryEntity);

            _dictionaryController.Put(_dictionaryEntity.Id, dictionaryViewModel);

            _unitOfWork.DictionariesRepository.Received().Update(Arg.Any<DictionaryEntity>());
        }

        [TestMethod]
        public void PutNotFound()
        {
            try
            {
                var dictionaryViewModel = Mapper.Map<DictionaryViewModel>(_dictionaryEntity);

                _dictionaryController.Put(6, dictionaryViewModel);

                Assert.Fail("Expected Exception was not thrown");
            }
            catch (HttpResponseException ex)
            {
                Assert.AreEqual(HttpStatusCode.NotFound, ex.Response.StatusCode);
            }
        }

        [TestMethod]
        public void PutNoPermission()
        {
            try
            {
                var dictionaryEntity = new Fixture().Create<DictionaryEntity>();
                dictionaryEntity.UserId = "100";
                _unitOfWork.DictionariesRepository.GetById(dictionaryEntity.Id).Returns(dictionaryEntity);

                var wordPairViewModel = Mapper.Map<DictionaryViewModel>(_dictionaryEntity);

                _dictionaryController.Put(dictionaryEntity.Id, wordPairViewModel); 
                
                Assert.Fail("Expected Exception was not thrown");
            }
            catch (HttpResponseException ex)
            {
                Assert.AreEqual(HttpStatusCode.Forbidden, ex.Response.StatusCode);
            }
        }
    }
}
