﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Web.Http;
using AutoMapper;
using EasyWords.Backend.Controllers;
using EasyWords.Backend.Database;
using EasyWords.Backend.Database.Entities;
using EasyWords.Backend.Models;
using EasyWords.Backend.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Ploeh.AutoFixture;

namespace EasyWords.Backend.Tests.Controllers
{
    [TestClass]
    public class WordsControllerTests
    {
        private WordsController _wordsController;
        private IUnitOfWork _unitOfWork;
        private EasyWordsContext _easyWordsContext;
        private IUserInfo _userInfo;
        private WordPairEntity _wordPairEntity;

        [TestInitialize]
        public void TestInitialize()
        {
            Bootstrapper.ConfigureAutoMapper();
            _easyWordsContext = Substitute.For<EasyWordsContext>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _userInfo = new UserInfoMock();
            _userInfo.UserId = "10";
            _wordsController = new WordsController(_unitOfWork, _userInfo);

            _wordPairEntity = new Fixture().Create<WordPairEntity>();
            _wordPairEntity.UserId = _userInfo.UserId;
            _unitOfWork.WordPairsRepository = Substitute.For<GenericRepository<WordPairEntity>>(_easyWordsContext);
            _unitOfWork.WordPairsRepository.GetById(_wordPairEntity.Id).Returns(_wordPairEntity);
            
        }

        [TestMethod]
        public void Get()
        {
            IQueryable<WordPairEntity> entites = new EnumerableQuery<WordPairEntity>(new List<WordPairEntity>() {_wordPairEntity}) ;
            _unitOfWork.WordPairsRepository.Where(Arg.Any<Expression<Func<WordPairEntity, bool>>>()).Returns(entites);

            var returnedEntities = _wordsController.Get();

            Assert.AreEqual(entites.ToList()[0].Id, returnedEntities.ToList()[0].Id);
            Assert.AreEqual(entites.ToList()[0].Word1, returnedEntities.ToList()[0].Word1);
            Assert.AreEqual(entites.ToList()[0].Word2, returnedEntities.ToList()[0].Word2);
        }
        
        [TestMethod]
        public void GetById()
        {
            _wordsController.Get(_wordPairEntity.Id);
            _unitOfWork.WordPairsRepository.Received().GetById(_wordPairEntity.Id);
        }

        [TestMethod]
        public void GetByIdNotFound()
        {
            try
            {
                _wordsController.Get(1);

                Assert.Fail("Expected Exception was not thrown");
            }
            catch (HttpResponseException ex)
            {
                Assert.AreEqual(HttpStatusCode.NotFound, ex.Response.StatusCode);
            }
        }

        [TestMethod]
        public void GetByIdNoPermission()
        {
            try
            {
                var wordPairEntity = new Fixture().Create<WordPairEntity>();
                wordPairEntity.UserId = "100";
                _unitOfWork.WordPairsRepository.GetById(wordPairEntity.Id).Returns(wordPairEntity);

                _wordsController.Get(wordPairEntity.Id);

                Assert.Fail("Expected Exception was not thrown");
            }
            catch (HttpResponseException ex)
            {
                Assert.AreEqual(HttpStatusCode.Forbidden, ex.Response.StatusCode);
            }
        }
        
        [TestMethod]
        public void Delete()
        {
           
            _wordsController.Delete(_wordPairEntity.Id);
            _unitOfWork.WordPairsRepository.Received().Delete(_wordPairEntity.Id);
        }

        [TestMethod]
        public void DeleteNotFound()
        {
            try
            {
                _wordsController.Delete(6);
                Assert.Fail("Expected Exception was not thrown");
            }
            catch (HttpResponseException ex)
            {
                Assert.AreEqual(HttpStatusCode.NotFound, ex.Response.StatusCode);
            }
        }

        [TestMethod]
        public void DeleteNoPermission()
        {
            var wordPairEntity = new Fixture().Create<WordPairEntity>();
            wordPairEntity.UserId = "100";
            _unitOfWork.WordPairsRepository.GetById(wordPairEntity.Id).Returns(wordPairEntity);

            try
            {
                _wordsController.Delete(wordPairEntity.Id);
            }
            catch (HttpResponseException ex)
            {
                Assert.AreEqual(HttpStatusCode.Forbidden, ex.Response.StatusCode);
            }
        }

        [TestMethod]
        public void Post()
        {
            var wordPair = new WordPairViewModel();

            _wordsController.Post(wordPair);
            _unitOfWork.WordPairsRepository.Received().Insert(Arg.Any<WordPairEntity>());
        }

        [TestMethod]
        public void PutAndCheckUpdate()
        {

            var wordPairViewModel = Mapper.Map<WordPairViewModel>(_wordPairEntity);

            _wordsController.Put(_wordPairEntity.Id, wordPairViewModel);

            _unitOfWork.WordPairsRepository.Received().Update(Arg.Any<WordPairEntity>());
        }

        [TestMethod]
        public void PutNotFound()
        {
            try
            {
                var wordPairViewModel = Mapper.Map<WordPairViewModel>(_wordPairEntity);

                _wordsController.Put(6, wordPairViewModel);

                Assert.Fail("Expected Exception was not thrown");
            }
            catch (HttpResponseException ex)
            {
                Assert.AreEqual(HttpStatusCode.NotFound, ex.Response.StatusCode);
            }
        }

        [TestMethod]
        public void PutNoPermission()
        {
            try
            {
                var wordPairEntity = new Fixture().Create<WordPairEntity>();
                wordPairEntity.UserId = "100";
                _unitOfWork.WordPairsRepository.GetById(wordPairEntity.Id).Returns(wordPairEntity);

                var wordPairViewModel = Mapper.Map<WordPairViewModel>(_wordPairEntity);

                _wordsController.Put(wordPairEntity.Id, wordPairViewModel);

                Assert.Fail("Expected Exception was not thrown");
            }
            catch (HttpResponseException ex)
            {
                Assert.AreEqual(HttpStatusCode.Forbidden, ex.Response.StatusCode);
            }
        }
    }
}
