﻿namespace EasyWords.Backend.Database.Entities
{
    public class WordPairEntity
    {
        public int Id { get; set; }

        public string Word1 { get; set; }

        public string Word2 { get; set; }

        public string UserId { get; set; }
    }
}