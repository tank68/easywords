﻿using EasyWords.Backend.Database.Entities;

namespace EasyWords.Backend.Database
{
    public interface IUnitOfWork
    {
        GenericRepository<WordPairEntity> WordPairsRepository { get; set; }

        GenericRepository<DictionaryEntity> DictionariesRepository { get; set; }

        void Save();
    }
}