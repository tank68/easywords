﻿using System.Data.Entity;
using EasyWords.Backend.Database.Entities;

namespace EasyWords.Backend.Database
{
    public class EasyWordsContext : DbContext
    {
        public EasyWordsContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<WordPairEntity> WordPairs { get; set; }

        public DbSet<DictionaryEntity> Dictionaries { get; set; }


    }
}