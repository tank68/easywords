﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;

namespace EasyWords.Backend.Models
{
    public class UserInfo : IUserInfo
    {
        public string UserId
        {
            get
            {
                return HttpContext.Current.User.Identity.GetUserId();
            }
            set {  }
        }
    }
}