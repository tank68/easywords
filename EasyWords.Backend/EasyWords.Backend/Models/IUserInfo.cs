﻿namespace EasyWords.Backend.Models
{
    public interface IUserInfo
    {
        string UserId { get; set; }
    }
}