﻿namespace EasyWords.Backend.Models
{
    public class UserInfoMock: IUserInfo
    {
        public string UserId { get; set; }
    }
}