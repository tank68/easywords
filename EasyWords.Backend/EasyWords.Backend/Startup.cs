﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EasyWords.Backend.Startup))]
namespace EasyWords.Backend
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
