using System.Web.Http;
using System.Web.Mvc;
using EasyWords.Backend.Controllers;
using EasyWords.Backend.Database;
using EasyWords.Backend.Database.Entities;
using EasyWords.Backend.Models;
using EasyWords.Backend.ViewModels;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using Unity.Mvc5;

namespace EasyWords.Backend
{
  public static class Bootstrapper
  {
    public static void Initialise()
    {
        ConfigureAutoMapper();

        InitialiseUnityContainer();
    }

      private static IUnityContainer InitialiseUnityContainer()
      {
          var container = BuildUnityContainer();
          
          GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);

          DependencyResolver.SetResolver(new UnityDependencyResolver(container));

          return container;
      }

      private static IUnityContainer BuildUnityContainer()
    {
      var container = new UnityContainer();

      // register all your components with the container here
      // it is NOT necessary to register your controllers

      // e.g. container.RegisterType<ITestService, TestService>();    
      RegisterTypes(container);

      return container;
    }

      public static void ConfigureAutoMapper()
      {
          AutoMapper.Mapper.CreateMap<DictionaryEntity, DictionaryViewModel>();
          AutoMapper.Mapper.CreateMap<DictionaryViewModel, DictionaryEntity>();
          AutoMapper.Mapper.CreateMap<WordPairEntity, WordPairViewModel>();
          AutoMapper.Mapper.CreateMap<WordPairViewModel, WordPairEntity>();
      }

      public static void RegisterTypes(IUnityContainer container)
    {
        container.RegisterType<IUnitOfWork, UnitOfWork>();
        container.RegisterType<IUserInfo, UserInfo>();
        container.RegisterType<AccountController>(new InjectionConstructor());
    }
  }
}