﻿using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using AutoMapper;
using EasyWords.Backend.Database;
using EasyWords.Backend.Database.Entities;
using EasyWords.Backend.Models;
using EasyWords.Backend.ViewModels;
using WebGrease.Css.Extensions;

namespace EasyWords.Backend.Controllers
{
    public class WordsController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;
        private IUserInfo _userManager;

        public WordsController(IUnitOfWork unitOfWork, IUserInfo userInfo)
        {
            _userInfo = userInfo;
            _unitOfWork = unitOfWork;
        }

        // GET: api/Words
        public IEnumerable<WordPairViewModel> Get()
        {
            var wordPairViewModels = new List<WordPairViewModel>();

            var wordPairModels = _unitOfWork.WordPairsRepository.Where(wp => wp.UserId == _userInfo.UserId);

            wordPairModels.ForEach(wpm => wordPairViewModels.Add(Mapper.Map<WordPairViewModel>(wpm)));

            return wordPairViewModels;
        }

        // GET: api/Words/5
        public WordPairViewModel Get(int id)
        {
            WordPairEntity wordPairEntity = _unitOfWork.WordPairsRepository.GetById(id);

            if (wordPairEntity == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            if (wordPairEntity.UserId != _userInfo.UserId)
            {
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            }

            return Mapper.Map<WordPairViewModel>(wordPairEntity);
        }

        // POST: api/Words
        public void Post([FromBody]WordPairViewModel value)
        {
            var wordPairEntity = Mapper.Map<WordPairEntity>(value);
            wordPairEntity.UserId = _userInfo.UserId;
            _unitOfWork.WordPairsRepository.Insert(wordPairEntity);
        }

        // PUT: api/Words/5
        public void Put(int id, [FromBody]WordPairViewModel value)
        {
            WordPairEntity wordPairEntity = _unitOfWork.WordPairsRepository.GetById(id);

            if (wordPairEntity == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            if (wordPairEntity.UserId !=  _userInfo.UserId)
            {
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            }

            wordPairEntity = Mapper.Map<WordPairEntity>(value);
            wordPairEntity.Id = id;
            wordPairEntity.UserId = _userInfo.UserId;

            _unitOfWork.WordPairsRepository.Update(wordPairEntity);
        }

        // DELETE: api/Words/5
        public void Delete(int id)
        {
            WordPairEntity wordPairEntity = _unitOfWork.WordPairsRepository.GetById(id);

            if (wordPairEntity == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            if (wordPairEntity.UserId != _userInfo.UserId)
            {
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            }

            _unitOfWork.WordPairsRepository.Delete(id);
        }

        public IUserInfo _userInfo { get; set; }
    }
}
