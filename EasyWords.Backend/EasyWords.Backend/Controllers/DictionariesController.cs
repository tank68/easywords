﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using AutoMapper;
using EasyWords.Backend.Database;
using EasyWords.Backend.Database.Entities;
using EasyWords.Backend.Models;
using EasyWords.Backend.ViewModels;
using WebGrease.Css.Extensions;

namespace EasyWords.Backend.Controllers
{
    public class DictionariesController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserInfo _userInfo;

        public DictionariesController(IUnitOfWork unitOfWork, IUserInfo userInfo)
        {
            _unitOfWork = unitOfWork;
            _userInfo = userInfo;
        }

        // GET: api/Dictionary
        public IEnumerable<DictionaryViewModel> Get()
        {
            var dictionaryViewModels = new List<DictionaryViewModel>();

            IEnumerable<DictionaryEntity> dictionaries = _unitOfWork.DictionariesRepository.Where(d => d.UserId == _userInfo.UserId);

            dictionaries.ForEach(d => dictionaryViewModels.Add(Mapper.Map<DictionaryViewModel>(d)));

            return dictionaryViewModels;
        }

        // GET: api/Dictionary/5
        public DictionaryViewModel Get(int id)
        {
            var dictionaryEntity = _unitOfWork.DictionariesRepository.GetById(id);

            if (dictionaryEntity == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            if (dictionaryEntity.UserId != _userInfo.UserId)
            {
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            }

            return Mapper.Map<DictionaryViewModel>(dictionaryEntity);
        }

        // POST: api/Dictionary
        public void Post([FromBody]DictionaryViewModel value)
        {
            _unitOfWork.DictionariesRepository.Insert(Mapper.Map<DictionaryEntity>(value));
        }

        // PUT: api/Dictionary/5
        public void Put(int id, [FromBody]DictionaryViewModel value)
        {
            DictionaryEntity dictionaryEntity = _unitOfWork.DictionariesRepository.GetById(id);

            if (dictionaryEntity == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            if (dictionaryEntity.UserId != _userInfo.UserId)
            {
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            }

            dictionaryEntity = Mapper.Map<DictionaryEntity>(value);
            dictionaryEntity.Id = id;
            dictionaryEntity.UserId = _userInfo.UserId;

            _unitOfWork.DictionariesRepository.Update(dictionaryEntity);
        }

        // DELETE: api/Dictionary/5
        public void Delete(int id)
        {
            var dictionaryEntity = _unitOfWork.DictionariesRepository.GetById(id);

            if (dictionaryEntity == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            if (dictionaryEntity.UserId != _userInfo.UserId)
            {
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            }

            _unitOfWork.DictionariesRepository.Delete(id);
        }
    }
}
