﻿namespace EasyWords.Api.ViewModels
{
    public class WordPairViewModel
    {
        public int Id { get; set; }

        public string Word1 { get; set; }

        public string Word2 { get; set; }
    }
}