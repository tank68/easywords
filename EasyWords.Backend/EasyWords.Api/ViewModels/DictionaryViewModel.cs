﻿namespace EasyWords.Api.ViewModels
{
    public class DictionaryViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }
    }
}