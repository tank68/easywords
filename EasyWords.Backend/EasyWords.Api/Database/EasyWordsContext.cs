﻿using System.Data.Entity;
using EasyWords.Api.Database.Entities;

namespace EasyWords.Api.Database
{
    public class EasyWordsContext : DbContext
    {
        public EasyWordsContext()
            : base("EasyWordsContext")
        {
        }

        public DbSet<WordPairEntity> WordPairs { get; set; }

        public DbSet<DictionaryEntity> Dictionaries { get; set; }


    }
}