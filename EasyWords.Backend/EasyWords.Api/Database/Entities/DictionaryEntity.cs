﻿namespace EasyWords.Api.Database.Entities
{
    public class DictionaryEntity
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string  Description { get; set; }

        public string UserId { get; set; }

        public int DictionaryId { get; set; }

    }
}