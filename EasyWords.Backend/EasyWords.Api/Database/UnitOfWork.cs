﻿using System;
using System.Data.Entity;
using EasyWords.Api.Database.Entities;

namespace EasyWords.Api.Database
{
    public class UnitOfWork : IDisposable, IUnitOfWork
    {
        DbContext _context = new EasyWordsContext();

        private GenericRepository<WordPairEntity> _wordPairsRepository;

        private GenericRepository<DictionaryEntity> _dictionariesRepository; 

        public GenericRepository<WordPairEntity> WordPairsRepository
        {
            get
            {
                return _wordPairsRepository ?? (_wordPairsRepository = new GenericRepository<WordPairEntity>(_context));
            }
            set { _wordPairsRepository = value; }
        }

        public GenericRepository<DictionaryEntity> DictionariesRepository
        {
            get
            {
                return _dictionariesRepository ?? (_dictionariesRepository = new GenericRepository<DictionaryEntity>(_context));
            }
            set { _dictionariesRepository = value; }
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}