﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace EasyWords.Api.Database
{
    public class AuthContext : IdentityDbContext<IdentityUser>
    {
        public AuthContext()
            : base("EasyWordsContext")
        {

        }
    }
}