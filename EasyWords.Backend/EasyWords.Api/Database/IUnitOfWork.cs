﻿using EasyWords.Api.Database.Entities;

namespace EasyWords.Api.Database
{
    public interface IUnitOfWork
    {
        GenericRepository<WordPairEntity> WordPairsRepository { get; set; }

        GenericRepository<DictionaryEntity> DictionariesRepository { get; set; }

        void Save();
    }
}