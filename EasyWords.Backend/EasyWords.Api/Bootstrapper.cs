using System.Web.Http;
using EasyWords.Api.Controllers;
using EasyWords.Api.Database;
using EasyWords.Api.Database.Entities;
using EasyWords.Api.Models;
using EasyWords.Api.ViewModels;
using Microsoft.Practices.Unity;
using Unity.WebApi;

namespace EasyWords.Api
{
  public static class Bootstrapper
  {
      public static void Initialise(HttpConfiguration config)
    {
        ConfigureAutoMapper();

        InitialiseUnityContainer(config);
    }

      public static IUnityContainer InitialiseUnityContainer(HttpConfiguration config)
      {
          var container = BuildUnityContainer();
          config.DependencyResolver = new UnityDependencyResolver(container);
          return container;
      }

      private static IUnityContainer BuildUnityContainer()
    {
      var container = new UnityContainer();

      // register all your components with the container here
      // it is NOT necessary to register your controllers

      // e.g. container.RegisterType<ITestService, TestService>();    
      RegisterTypes(container);

      return container;
    }

      public static void ConfigureAutoMapper()
      {
          AutoMapper.Mapper.CreateMap<DictionaryEntity, DictionaryViewModel>();
          AutoMapper.Mapper.CreateMap<DictionaryViewModel, DictionaryEntity>();
          AutoMapper.Mapper.CreateMap<WordPairEntity, WordPairViewModel>();
          AutoMapper.Mapper.CreateMap<WordPairViewModel, WordPairEntity>();
      }

      public static void RegisterTypes(IUnityContainer container)
    {
        container.RegisterType<IUnitOfWork, UnitOfWork>();
        container.RegisterType<IUserInfo, UserInfo>();
        container.RegisterType<AccountController>(new InjectionConstructor());
    }
  }
}