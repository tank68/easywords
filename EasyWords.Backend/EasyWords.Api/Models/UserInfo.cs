﻿using System.Web;
using Microsoft.AspNet.Identity;

namespace EasyWords.Api.Models
{
    public class UserInfo : IUserInfo
    {
        public string UserId
        {
            get
            {
                return HttpContext.Current.User.Identity.GetUserId();
            }
            set {  }
        }
    }
}