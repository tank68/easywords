﻿namespace EasyWords.Api.Models
{
    public interface IUserInfo
    {
        string UserId { get; set; }
    }
}