﻿namespace EasyWords.Api.Models
{
    public class UserInfoMock: IUserInfo
    {
        public string UserId { get; set; }
    }
}