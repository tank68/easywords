﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using AutoMapper;
using EasyWords.Api.Database;
using EasyWords.Api.Database.Entities;
using EasyWords.Api.Models;
using EasyWords.Api.ViewModels;

namespace EasyWords.Api.Controllers
{
    public class DictionariesController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserInfo _userInfo;

        public DictionariesController(IUnitOfWork unitOfWork, IUserInfo userInfo)
        {
            _unitOfWork = unitOfWork;
            _userInfo = userInfo;
        }

        // GET: api/Dictionary
        public IEnumerable<DictionaryViewModel> Get()
        {
            var dictionaryViewModels = new List<DictionaryViewModel>();

            List<DictionaryEntity> dictionaries = _unitOfWork.DictionariesRepository.Where(d => d.UserId == _userInfo.UserId).ToList();

            dictionaries.ForEach(d => dictionaryViewModels.Add(Mapper.Map<DictionaryViewModel>(d)));

            return dictionaryViewModels;
        }

        // GET: api/Dictionary/5
        public DictionaryViewModel Get(int id)
        {
            var dictionaryEntity = _unitOfWork.DictionariesRepository.GetById(id);

            if (dictionaryEntity == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            if (dictionaryEntity.UserId != _userInfo.UserId)
            {
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            }

            return Mapper.Map<DictionaryViewModel>(dictionaryEntity);
        }

        // POST: api/Dictionary
        public void Post([FromBody]DictionaryViewModel value)
        {
            _unitOfWork.DictionariesRepository.Insert(Mapper.Map<DictionaryEntity>(value));
        }

        // PUT: api/Dictionary/5
        public void Put(int id, [FromBody]DictionaryViewModel value)
        {
            DictionaryEntity dictionaryEntity = _unitOfWork.DictionariesRepository.GetById(id);

            if (dictionaryEntity == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            if (dictionaryEntity.UserId != _userInfo.UserId)
            {
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            }

            dictionaryEntity = Mapper.Map<DictionaryEntity>(value);
            dictionaryEntity.Id = id;
            dictionaryEntity.UserId = _userInfo.UserId;

            _unitOfWork.DictionariesRepository.Update(dictionaryEntity);
        }

        // DELETE: api/Dictionary/5
        public void Delete(int id)
        {
            var dictionaryEntity = _unitOfWork.DictionariesRepository.GetById(id);

            if (dictionaryEntity == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            if (dictionaryEntity.UserId != _userInfo.UserId)
            {
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            }

            _unitOfWork.DictionariesRepository.Delete(id);
        }
    }
}
