﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using AutoMapper;
using EasyWords.Api.Database;
using EasyWords.Api.Database.Entities;
using EasyWords.Api.Models;
using EasyWords.Api.ViewModels;
using Microsoft.AspNet.Identity;

namespace EasyWords.Api.Controllers
{
    [RoutePrefix("words")]
    public class WordsController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;
        private IUserInfo _userManager;

        public WordsController(IUnitOfWork unitOfWork, IUserInfo userInfo)
        {
            _userInfo = userInfo;
            _unitOfWork = unitOfWork;
        }

        // GET: api/Words
        [Route("getwords")]
        public IEnumerable<WordPairViewModel> GetWords()
        {
            var wordPairViewModels = new List<WordPairViewModel>();

            var wordPairModels = _unitOfWork.WordPairsRepository.Where(wp => wp.UserId == _userInfo.UserId).ToList();

            wordPairModels.ForEach(wpm => wordPairViewModels.Add(Mapper.Map<WordPairViewModel>(wpm)));

            return wordPairViewModels;
        }

        // GET: api/Words/5
        public WordPairViewModel Get(int id)
        {
            WordPairEntity wordPairEntity = _unitOfWork.WordPairsRepository.GetById(id);

            if (wordPairEntity == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            if (wordPairEntity.UserId != _userInfo.UserId)
            {
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            }

            return Mapper.Map<WordPairViewModel>(wordPairEntity);
        }

        // POST: api/Words
        [Route("addword")]
        [HttpPost]
        public void AddWord([FromBody]WordPairViewModel value)
        {
            var wordPairEntity = Mapper.Map<WordPairEntity>(value);

            wordPairEntity.UserId = _userInfo.UserId;
            _unitOfWork.WordPairsRepository.Insert(wordPairEntity);
        }

        // PUT: api/Words/5
        public void Put(int id, [FromBody]WordPairViewModel value)
        {
            WordPairEntity wordPairEntity = _unitOfWork.WordPairsRepository.GetById(id);

            if (wordPairEntity == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            if (wordPairEntity.UserId !=  _userInfo.UserId)
            {
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            }

            wordPairEntity = Mapper.Map<WordPairEntity>(value);
            wordPairEntity.Id = id;
            wordPairEntity.UserId = _userInfo.UserId;

            _unitOfWork.WordPairsRepository.Update(wordPairEntity);
        }

        // DELETE: api/Words/5
        public void Delete(int id)
        {
            WordPairEntity wordPairEntity = _unitOfWork.WordPairsRepository.GetById(id);

            if (wordPairEntity == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            if (wordPairEntity.UserId != _userInfo.UserId)
            {
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            }

            _unitOfWork.WordPairsRepository.Delete(id);
        }

        public IUserInfo _userInfo { get; set; }
    }
}
