var app = angular.module('EasyWordsApp', ['ngRoute', 'LocalStorageModule', 'angular-loading-bar']);

app.config(function ($routeProvider, $httpProvider) {
    $httpProvider.interceptors.push('authInterceptorService');

    $routeProvider.when("/home", {
        controller: "homeController",
        templateUrl: "/EasyWords.FrontEnd/app/views/home.html"
    });

    $routeProvider.when("/login", {
        controller: "loginController",
        templateUrl: "/EasyWords.FrontEnd/app/views/login.html"
    });

    $routeProvider.when("/signup", {
        controller: "signupController",
        templateUrl: "/EasyWords.FrontEnd/app/views/signup.html"
    });

    $routeProvider.when("/orders", {
        controller: "ordersController",
        templateUrl: "/EasyWords.FrontEnd/app/views/orders.html"
    });

    $routeProvider.otherwise({ redirectTo: "/home" });
});

app.run(['authService', function (authService) {
    authService.fillAuthData();
}]);